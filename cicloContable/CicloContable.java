/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cicloContable;

/**
 *
 * @author KAROL_PC
 */
import java.util.Arrays;
import java.util.Date;      
import java.text.DateFormatSymbols;
import java.time.chrono.ThaiBuddhistChronology;
public class CicloContable {

    private int tipoCicloContable; // 1. Anual 2.Mensual
    private String tipoCicloString; // "Anual" o "Mensual"
    public String nombreCiclo;
    private boolean Aperturado;
    private Date fechaAperturado;
    private boolean Cerrado;
    private Date fechaCerrado;
    private CicloContable[] ciclosInternos;//Unicamente se llena cuando el objeto de ciclo contable e anual, ya que este arregloguardara sus ciclos contables mensuales

    public CicloContable(){
        this.Aperturado = false;
        this.Cerrado = false;
    }

    public CicloContable(int tipoCicloContable, String tipoCicloString) {
        this.tipoCicloContable = tipoCicloContable;
        this.tipoCicloString = tipoCicloString;
    }

    public void setNombreCiclo(String nombreCiclo){
        this.nombreCiclo = nombreCiclo;
    }

    public String gettipoCicloString(){
        return this.tipoCicloString;
    }

    public CicloContable[] getciclosInternos(){
        return this.ciclosInternos;
    }

    public void CerrarCiclo() {
        if (this.Aperturado){
            this.Cerrado = true;
            this.fechaCerrado = new Date();
        }
    }

    public void AperturarCiclo() {
        this.Aperturado = true;
        this.fechaAperturado = new Date();
        if(this.tipoCicloString.equals("Mensual")){
            this.nombreCiclo = String.valueOf(getMonthForInt(fechaAperturado.getMonth())) + " - " +(fechaAperturado.getYear()+1900);
        }else if(this.tipoCicloString.equals("Anual")){
            this.nombreCiclo = "Año - " +(fechaAperturado.getYear()+1900);
        }
    }

    public void AnadirCicloInterno(CicloContable c){
        if(this.tipoCicloString.equals("Anual")){
            if(this.ciclosInternos == null){
                ciclosInternos = new CicloContable[1];
                ciclosInternos[0] = c;
            }else{
                ciclosInternos =  Arrays.copyOf(ciclosInternos, c.ciclosInternos.length + 1);
                ciclosInternos[c.ciclosInternos.length - 1] = c;
            }
        }
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }


}