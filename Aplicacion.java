
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.Socket;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;
import menu.Catalogo;
import cicloContable.CicloContable;
import pojo.Cuenta;

/**
 *
 * @author KAROL_PC
 */
public class Aplicacion {

    /**
     * @throws java.lang.Exception
     */
    static String NombreInstitucion;
    static boolean CicloNuevoIniciadoAnual;// Indica que se ha iniciado un nuevo ciclo contable si es igual a true; se
    // cambia a false cuando se cierre el ciclo contable
    static boolean CicloNuevoIniciadoMensual;
    static CicloContable[] ciclosContablesHistorial;
    static CicloContable cicloAnualActual;
    static CicloContable cicloMensualActual;

    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int seguir = 1;
        Scanner sc = new Scanner(System.in);

        Catalogo m = new Catalogo();
        // m.cargarCuentasdelSistema();
        // m.agregarCuentasUsuario();
        int j = 0;
        while (seguir == 1) {
            try {
                // Cargando(1);
                if (NombreInstitucion == null) {
                    System.out.println("BIENVENIDOS AL SISTEMA DE REGISTRO CONTABLE PARA INSTITUCIONES ECONÓMICAS");
                    // Cargando(2);
                    System.out.println("Para empezar..");
                    System.out.print("Nombre de su institución: ");
                    NombreInstitucion = sc.nextLine();
                    NombreInstitucion = NombreInstitucion.replace("  ", " ");
                    System.out.println("Nuevo nombre de institución : '" + NombreInstitucion + "'.");
                    // Cargando(2);
                }

                System.out.print("            _______________________________");
                for (int i = 0; i < NombreInstitucion.length(); i++) {
                    System.out.print("_");
                }
                System.out.println("");
                System.out.println("            SISTEMA DE REGISTRO CONTABLE - " + NombreInstitucion.toUpperCase());
                System.out.print("            _______________________________");
                for (int i = 0; i < NombreInstitucion.length(); i++) {
                    System.out.print("_");
                }
                System.out.println("");
                int opcMenuPrincipal = 1;

                do {
                    System.out.println("                        ACCIONES DEL SISTEMA");
                    System.out.println("");
                    System.out.println("                    1. Apertura Ciclo Contable Anual");
                    System.out.println("                    2. Apertura Ciclo Contable Mensual");
                    System.out.println("                    3. Ver estados financieros - Ciclo Mensual Actual");
                    System.out.println("                    4. Ver estados financieros - Ciclos pasados");
                    System.out.println("                    5. Cierre Ciclo Contable - Ciclo Actual");
                    System.out.println("                    6. Configuración del Sistema");
                    System.out.println("                    Cualquier otro. Salir del sistema");
                    System.out.print("Elegir: ");
                    opcMenuPrincipal = sc.nextInt();

                    switch (opcMenuPrincipal) {
                    case 1: {
                        if (CicloNuevoIniciadoAnual == false) {
                            if (m.getCuentasUsuarioFinal() != null) {
                                if (m.CatalogoLleno()) {
                                    System.out.println("                    NUEVO CICLO CONTABLE ANUAL");
                                    CicloContable cc = new CicloContable(1, "Anual");
                                    cc.AperturarCiclo();
                                    System.out.print("Id de ciclo contable: ");
                                    System.out.println(cc.nombreCiclo);
                                    System.out.println("");
                                    System.out.println("¿GUARDAR?");
                                    System.out.println("1. SI  2. Especificar  3. Cancelar");
                                    int opc = sc.nextInt();

                                    switch (opc) {
                                    case 1: {
                                        cicloAnualActual = cc;
                                        AnhadirCiclosContablesAlHistorial(cicloAnualActual);
                                        CicloNuevoIniciadoAnual = true;
                                        JOptionPane.showMessageDialog(null,
                                                "!Enhorabuena!\nCiclo contable anual creado", "Éxito",
                                                JOptionPane.INFORMATION_MESSAGE);
                                        break;
                                    }

                                    case 2: {
                                        System.out.print("Escriba el id del ciclo contable: ");
                                        sc.nextLine();
                                        String nombreCiclo = sc.nextLine();

                                        System.out.println("¿GUARDAR?");
                                        System.out.println("1. SI  2. Cancelar");
                                        int opc2 = sc.nextInt();

                                        switch (opc2) {
                                        case 1: {
                                            cc.setNombreCiclo(nombreCiclo);
                                            cicloAnualActual = cc;
                                            AnhadirCiclosContablesAlHistorial(cicloAnualActual);
                                            CicloNuevoIniciadoAnual = true;
                                            break;
                                        }

                                        case 2: {
                                            System.out.println("Cancelado");
                                            break;
                                        }
                                        }

                                        break;
                                    }

                                    case 3: {
                                        System.out.println("Cancelado");
                                        break;
                                    }
                                    }

                                } else {
                                    JOptionPane.showMessageDialog(null,
                                            "!Catálogo de cuentas no cuenta con suficientes cuentas!", "Error",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "!El manual contable aún no ha sido especificado.!", "Error",
                                        JOptionPane.WARNING_MESSAGE);
                            }

                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "!Actualmente existe un ciclo contable anual aperturado!", "Error",
                                    JOptionPane.WARNING_MESSAGE);
                        }

                        break;
                    }

                    case 2: {
                        if (CicloNuevoIniciadoMensual == false) {
                            if (m.getCuentasUsuarioFinal() != null) {
                                if (m.CatalogoLleno()) {
                                    System.out.println("                    NUEVO CICLO CONTABLE MENSUAL");
                                    CicloContable cc = new CicloContable(1, "Mensual");
                                    cc.AperturarCiclo();
                                    System.out.print("Id de ciclo contable: ");
                                    System.out.println(cc.nombreCiclo);
                                    System.out.println("");
                                    System.out.println("¿GUARDAR?");
                                    System.out.println("1. SI  2. Especificar  3. Cancelar");
                                    int opc = sc.nextInt();

                                    switch (opc) {
                                    case 1: {
                                        CicloNuevoIniciadoMensual = true;
                                        JOptionPane.showMessageDialog(null,
                                                "!Enhorabuena!\nCiclo contable mensual creado.", "Éxito",
                                                JOptionPane.INFORMATION_MESSAGE);
                                        break;
                                    }

                                    case 2: {
                                        System.out.print("Escriba el id del ciclo contable: ");
                                        sc.nextLine();
                                        String nombreCiclo = sc.nextLine();

                                        System.out.println("¿GUARDAR?");
                                        System.out.println("1. SI  2. Cancelar");
                                        int opc2 = sc.nextInt();

                                        switch (opc2) {
                                        case 1: {
                                            cc.setNombreCiclo(nombreCiclo);
                                            CicloNuevoIniciadoAnual = true;
                                            break;
                                        }

                                        case 2: {
                                            System.out.println("Cancelado");
                                            break;
                                        }
                                        }

                                        break;
                                    }

                                    case 3: {
                                        System.out.println("Cancelado");
                                        break;
                                    }
                                    }

                                } else {
                                    JOptionPane.showMessageDialog(null,
                                            "!Catálogo de cuentas no cuenta con suficientes cuentas!", "Error",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "!El manual contable aún no ha sido especificado.!", "Error",
                                        JOptionPane.WARNING_MESSAGE);
                            }

                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "!Actualmente existe un ciclo contable mensual aperturado!", "Error",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                        break;
                    }

                    case 3: {

                        break;
                    }

                    case 4: {
                        CicloNuevoIniciadoAnual = false;
                        break;
                    }

                    case 5: {

                        break;
                    }

                    case 6: {
                        int opcConfiguracion = 0;
                        ;
                        do {
                            if (opcConfiguracion != 2) {
                                Cargando(1);
                            }
                            System.out.println("");
                            System.out.println("                        CONFIGURACIÓN DEL SISTEMA");
                            System.out.println("");
                            System.out.println("                    1. Gestionar Manual Contable.");
                            System.out.println("                    2. Ver Manual Contable");
                            System.out.println("                    Cualquier otro. Regresar al Menú Principal.");
                            try {
                                opcConfiguracion = sc.nextInt();
                            } catch (NumberFormatException e) {
                                System.err.println("Volviendo al menú principal");
                                Cargando(1);
                            }

                            switch (opcConfiguracion) {
                            case 1: {
                                System.out.println("                        CONFIGURACIÓN DEL SISTEMA");
                                System.out.println("                        1. Agregar cuentas.");
                                System.out.println("                        2. Eliminar cuentas");
                                int opcCuentasModificar = sc.nextInt();
                                m.cargarCuentasdelSistema();
                                switch (opcCuentasModificar) {
                                case 1: {
                                    Cargando(1);
                                    m.mostrarCuentasdelSistema();
                                    m.agregarCuentasUsuario();
                                    break;
                                }

                                case 2: {
                                    boolean[] clasificaciones = new boolean[10];
                                    boolean[] cabecerasClasificaciones = new boolean[10];
                                    Cuenta[] cuentas = m.getCuentasUsuarioFinal();
                                    m.MostrarManualContable(cuentas, clasificaciones, clasificaciones);
                                    if (cuentas != null) {
                                        m.eliminarCuentasUsuario();
                                    }
                                    break;
                                }
                                }

                                break;
                            }

                            case 2: {
                                boolean[] clasificaciones = new boolean[10];
                                boolean[] cabecerasClasificaciones = new boolean[10];
                                Cuenta[] cuentas = m.getCuentasUsuarioFinal();
                                m.MostrarManualContable(cuentas, clasificaciones, clasificaciones);
                                System.out.println("Presiona enter para volver...");
                                sc.nextLine();
                                String tecla = sc.nextLine();
                                opcConfiguracion = 0;
                                break;
                            }

                            default: {
                                System.err.println("Volviendo al menú principal");
                                Cargando(1);
                                opcConfiguracion = 3;
                                break;
                            }
                            }

                            if (opcConfiguracion == 3) {
                                break;
                            }
                        } while (opcConfiguracion < 1 || opcMenuPrincipal > 3);

                        break;
                    }
                    default: {
                        System.out.println("                          Saliendo del sistema");
                        // Cargando(1);
                        System.out.println("                                ¿Cancelar?");
                        System.out.println("                           1. SI    Cualquiera. NO");
                        seguir = sc.nextInt();
                    }
                    }
                } while (opcMenuPrincipal < 1 || opcMenuPrincipal > 4);

                if (seguir == 1) {
                    System.out.println("                             Continuando en el sistema");
                    Cargando(1);
                }
            } catch (InputMismatchException e) {
                System.out.println("                          Saliendo del sistema");
                Cargando(1);
                break;
            }
        }
    }

    static public void Cargando(int veces) throws Exception {
        Thread main = new Thread();
        for (int j = 0; j < veces; j++) {
            System.out.print("\033[H\033[2J");
            System.out.flush();

            System.out.print("                             ");
            for (int i = 0; i < 10; i++) {
                main.sleep(250);
                System.out.print(".");
            }
            main.sleep(500);
        }
    }

    static public void AnhadirCiclosContablesAlHistorial(CicloContable cc) {
        if (cc.gettipoCicloString().equals("Anual")) {
            if (ciclosContablesHistorial == null || ciclosContablesHistorial.length == 0) {
                ciclosContablesHistorial = new CicloContable[1];
                ciclosContablesHistorial[0] = cc;
            } else if (ciclosContablesHistorial.length >= 1) {
                ciclosContablesHistorial = Arrays.copyOf(ciclosContablesHistorial, ciclosContablesHistorial.length + 1);
                ciclosContablesHistorial[ciclosContablesHistorial.length - 1] = cc;
            }
        } else if (cc.gettipoCicloString().equals("Mensual")) {
            if (ciclosContablesHistorial.length >= 1) {
                CicloContable cicloActual = ciclosContablesHistorial[ciclosContablesHistorial.length - 1];
                cicloActual.AnadirCicloInterno(cc);
            }
        }

    }

}
