/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.Arrays;
import java.util.Scanner;
import javax.swing.JOptionPane;
import pojo.Cuenta;

/**
 *
 * @author KAROL_PC
 */
public class Catalogo {

    Cuenta[] cuentasSistema = new Cuenta[73];
    private Cuenta[] cuentasUsuarioTemp = new Cuenta[1];
    private int cuentasUsuarioFinalLength;
    private boolean[] clasificacionesTemp;
    private boolean[] cabecerasClasificacionTemp = new boolean[10];
    private boolean[] clasificacionesFinal;
    private boolean[] cabecerasClasificacionFinal = new boolean[10];
    private Cuenta[] cuentasUsuarioFinal;
    boolean[] ClasificacionesGenerales = new boolean[3];// Verifica que el usuario haya llenado su catalogo de cuentas
    // con al menos una cuenta en cada clasificacion general como lo
    // son activos, pasivos y capiital
    String[] categoria = { "Activo Circulante Disponible", "Activo Circulante Realizable",
            "Activo No Circulante (Fijo)", "Activo No Circulante (Intangible)", "Activo No Circulante (Otros Activos)",
            "Pasivo Circulante A Corto plazo", "Pasivo No Circulante (Fijo)", "Pasivo No Circualnte Diferido",
            "Capital Contribuido", "Capital Ganado" };
    String[] numero = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
    static Scanner sc = new Scanner(System.in);

    // ****ACTIVOS****
    Cuenta c1 = new Cuenta("1001", "Caja General                                              ", 1);
    Cuenta c2 = new Cuenta("1002", "Caja Chica                                                ", 1);
    Cuenta c3 = new Cuenta("1003", "Fondo de Oportunidades                                    ", 1);
    Cuenta c4 = new Cuenta("1004", "Banco                                                     ", 1);
    Cuenta c5 = new Cuenta("1005", "Inversiones Temporales                                    ", 1);
    // Activo Circulante Disponible.

    Cuenta c6 = new Cuenta("1006", "Clientes                                                  ", 2);
    Cuenta c7 = new Cuenta("1007", "Documentos por Cobrar                                     ", 2);
    Cuenta c8 = new Cuenta("1008", "Deudores                                                  ", 2);
    Cuenta c9 = new Cuenta("1009", "Funcionarios y Empleados                                  ", 2);
    Cuenta c10 = new Cuenta("1010", "IVA Acreditable                                           ", 2);
    Cuenta c11 = new Cuenta("1011", "Anticipo de Impuestos                                     ", 2);
    Cuenta c12 = new Cuenta("1012", "Inventarios                                               ", 2);
    Cuenta c13 = new Cuenta("1013", "Mercancías en tránsito                                    ", 2);
    Cuenta c14 = new Cuenta("1014", "Anticipo a Provedores                                     ", 2);
    Cuenta c15 = new Cuenta("1015", "Papelería y Útiles                                        ", 2);
    Cuenta c16 = new Cuenta("1016", "Propaganda y Publicidad                                   ", 2);
    Cuenta c17 = new Cuenta("1017", "Muestras Médicas y Literaturas                            ", 2);
    Cuenta c18 = new Cuenta("1018", "Primas De Seguros y Fianzas                               ", 2);
    Cuenta c19 = new Cuenta("1019", "Rentas Pagadas por Anticipado                             ", 2);
    Cuenta c20 = new Cuenta("1020", "Intereses Pagados por Anticipado                          ", 2);
    // Activo Circulante Realizable.

    Cuenta c21 = new Cuenta("1021", "Terreno                                              ", 3);
    Cuenta c22 = new Cuenta("1022", "Edificio                                             ", 3);
    Cuenta c23 = new Cuenta("1023", "Maquinaria                                           ", 3);
    Cuenta c24 = new Cuenta("1024", "Mobiliario y Equipo de Oficina                       ", 3);
    Cuenta c25 = new Cuenta("1025", "Muebles y Enseres                                    ", 3);
    Cuenta c26 = new Cuenta("1026", "Equipo de Transporte                                 ", 3);
    Cuenta c27 = new Cuenta("1027", "Equipo de Entrega Y Reparto                          ", 3);
    // Activo No Circulante (Fijo).

    Cuenta c28 = new Cuenta("1028", "Derechos de Autor                                         ", 4);
    Cuenta c29 = new Cuenta("1029", "Patentes                                                  ", 4);
    Cuenta c30 = new Cuenta("1030", "Marcas Registradas                                        ", 4);
    Cuenta c31 = new Cuenta("1031", "Nombres Comerciales                                       ", 4);
    Cuenta c32 = new Cuenta("1032", "Crédito Comercial                                         ", 4);
    Cuenta c33 = new Cuenta("1033", "Gastos Preoperativos                                      ", 4);
    Cuenta c34 = new Cuenta("1034", "Descuento en Emisión de Obligaciones                      ", 4);
    Cuenta c35 = new Cuenta("1035", "Gastos en Colocacón de Valores                            ", 4);
    Cuenta c36 = new Cuenta("1036", "Gastos de Constitución                                    ", 4);
    Cuenta c37 = new Cuenta("1037", "Gastos de Organización                                    ", 4);
    Cuenta c38 = new Cuenta("1038", "Gastos de Instalación                                     ", 4);
    Cuenta c39 = new Cuenta("1039", "Franquicias                                               ", 4);
    // Activo No Circulante (Intangible).

    Cuenta c40 = new Cuenta("1040", "Fondo de Amortización de Obligaciones                     ", 5);
    Cuenta c41 = new Cuenta("1041", "Depósitos en Garantias                                    ", 5);
    Cuenta c42 = new Cuenta("1042", "Inversiones en proceso                                    ", 5);
    Cuenta c43 = new Cuenta("1043", "Terrenos no Utilizados                                    ", 5);
    Cuenta c44 = new Cuenta("1044", "Maquinaria no Utilizada                                   ", 5);
    // Activo No Circulante (Otros Activos).

    // ****PASIVOS****.
    Cuenta c45 = new Cuenta("1045", "Proveedores o Cuentas por Pagar                           ", 6);
    Cuenta c46 = new Cuenta("1046", "Documentos por Pagar                                      ", 6);
    Cuenta c47 = new Cuenta("1047", "Acreedores Diversos                                       ", 6);
    Cuenta c48 = new Cuenta("1048", "Acreedores Bancarios                                      ", 6);
    Cuenta c49 = new Cuenta("1049", "Anticipo de Clientes                                      ", 6);
    Cuenta c50 = new Cuenta("1050", "Dividendos por Pagar                                      ", 6);
    Cuenta c51 = new Cuenta("1051", "IVA por Pagar                                             ", 6);
    Cuenta c52 = new Cuenta("1052", "Impuestos Sobre la Renta por Pagar                        ", 6);
    Cuenta c53 = new Cuenta("1053", "Impuestos y Derechos Retenidos por Enterar                ", 6);
    Cuenta c54 = new Cuenta("1054", "Intereses por Pagar                                       ", 6);
    Cuenta c55 = new Cuenta("1055", "Sueldos Acumulados Por Pagar                              ", 6);
    Cuenta c56 = new Cuenta("1056", "Gastos Acumulados por Pagar                               ", 6);
    Cuenta c57 = new Cuenta("1057", "Rentas Cobradas Por Anticipado                            ", 6);
    Cuenta c58 = new Cuenta("1058", "Intereses Cobrados por Anticipado                         ", 6);
    // Pasivo Circulante o A corto Plazo.

    Cuenta c59 = new Cuenta("1059", "Acreedores Hipotecarios                                    ", 7);
    Cuenta c60 = new Cuenta("1060", "Acreedores Bancarios                                       ", 7);
    Cuenta c61 = new Cuenta("1061", "Documentos por Pagar a Largo Plazo                         ", 7);
    Cuenta c62 = new Cuenta("1062", "Obligaciones en circulación                                ", 7);
    Cuenta c63 = new Cuenta("1063", "Bonos por Pagar                                            ", 7);
    // Pasivo No Circulante o A Largo Plazo (Fijo).

    Cuenta c64 = new Cuenta("1064", "Rentas Cobradas por Anticipado                            ", 8);
    Cuenta c65 = new Cuenta("1065", "Intereses Cobrados Por AnticiPado                         ", 8);
    // Pasivos No Circulante o A Largo Plazo (Diferido)

    // ****CAPITAL CONTABLE****
    Cuenta c66 = new Cuenta("1066", "Capital Social                                            ", 9);
    Cuenta c67 = new Cuenta("1067", "Aportaciones Para Futuros Aumentos de Capital             ", 9);
    Cuenta c68 = new Cuenta("1068", "Prima en Venta de Acciones                                ", 9);
    Cuenta c69 = new Cuenta("1069", "Donaciones                                                ", 9);
    // CAPITAL CONTRIBUIDO

    Cuenta c70 = new Cuenta("1070", "Utilidades Retenidas                                      ", 10);
    Cuenta c71 = new Cuenta("1071", "Pérdidas Acumuladas                                        ", 10);
    Cuenta c72 = new Cuenta("1072", "Exceso o Insuficiencia en la Actualización del Capital     ", 10);
    Cuenta c73 = new Cuenta("1073", "Reserva Legal                                              ", 10);
    // CAPITAL GANADO (DÉFICIT)

    public Cuenta[] getCuentasUsuarioFinal() {
        return cuentasUsuarioFinal;
    }

    public boolean[] getClasificacionesFinal() {
        return clasificacionesFinal;
    }

    public Catalogo() {
    }

    public void cargarCuentasdelSistema() {

        // ****ACTIVOS****
        cuentasSistema[0] = c1;
        cuentasSistema[1] = c2;
        cuentasSistema[2] = c3;
        cuentasSistema[3] = c4;
        cuentasSistema[4] = c5;
        // Activo Circulante Disponible.

        cuentasSistema[5] = c6;
        cuentasSistema[6] = c7;
        cuentasSistema[7] = c8;
        cuentasSistema[8] = c9;
        cuentasSistema[9] = c10;
        cuentasSistema[10] = c11;
        cuentasSistema[11] = c12;
        cuentasSistema[12] = c13;
        cuentasSistema[13] = c14;
        cuentasSistema[14] = c15;
        cuentasSistema[15] = c16;
        cuentasSistema[16] = c17;
        cuentasSistema[17] = c18;
        cuentasSistema[18] = c19;
        cuentasSistema[19] = c20;
        // Activo Circulante Realizable.

        cuentasSistema[20] = c21;
        cuentasSistema[21] = c22;
        cuentasSistema[22] = c23;
        cuentasSistema[23] = c24;
        cuentasSistema[24] = c25;
        cuentasSistema[25] = c26;
        cuentasSistema[26] = c27;
        // Activo No Circulante (Fijo).

        cuentasSistema[27] = c28;
        cuentasSistema[28] = c29;
        cuentasSistema[29] = c30;
        cuentasSistema[30] = c31;
        cuentasSistema[31] = c32;
        cuentasSistema[32] = c33;
        cuentasSistema[33] = c34;
        cuentasSistema[34] = c35;
        cuentasSistema[35] = c36;
        cuentasSistema[36] = c37;
        cuentasSistema[37] = c38;
        cuentasSistema[38] = c39;
        // Activo No Circulante (Intangible).

        cuentasSistema[39] = c40;
        cuentasSistema[40] = c41;
        cuentasSistema[41] = c42;
        cuentasSistema[42] = c43;
        cuentasSistema[43] = c44;
        // Activo No Circulante (Otros Activos).

        // ****PASIVOS****.
        cuentasSistema[44] = c45;
        cuentasSistema[45] = c46;
        cuentasSistema[46] = c47;
        cuentasSistema[47] = c48;
        cuentasSistema[48] = c49;
        cuentasSistema[49] = c50;
        cuentasSistema[50] = c51;
        cuentasSistema[51] = c52;
        cuentasSistema[52] = c53;
        cuentasSistema[53] = c54;
        cuentasSistema[54] = c55;
        cuentasSistema[55] = c56;
        cuentasSistema[56] = c57;
        cuentasSistema[57] = c58;
        // Pasivo Circulante o A corto Plazo.

        cuentasSistema[58] = c59;
        cuentasSistema[59] = c60;
        cuentasSistema[60] = c61;
        cuentasSistema[61] = c62;
        cuentasSistema[62] = c63;
        // Pasivo No Circulante o A Largo Plazo (Fijo).

        cuentasSistema[63] = c64;
        cuentasSistema[64] = c65;
        // Pasivos No Circulante o A Largo Plazo (Diferido)

        // ****CAPITAL CONTABLE****
        cuentasSistema[65] = c66;
        cuentasSistema[66] = c67;
        cuentasSistema[67] = c68;
        cuentasSistema[68] = c69;
        // Pasivo No Circulante o A Largo Plazo (Fijo).

        cuentasSistema[69] = c70;
        cuentasSistema[70] = c71;
        cuentasSistema[71] = c72;
        cuentasSistema[72] = c73;
        // CAPITAL GANADO (DÉFICIT)

    }

    public void mostrarCuentasdelSistema() {
        // IMPRIMIR CUENTAS
        System.out.println("                           *****CATALOGO DE CUENTA*****                    ");
        RetornarCatalogo(1);// Activos
        RetornarCatalogo(0);// Cabecera de tabla

        for (int i = 0; i < cuentasSistema.length; i++) {

            if (cuentasSistema[i].getClasificacion() == 1) {
                numero[0] = categoria[0];
                System.out.println("|                            " + numero[0] + "                          |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (i = 0; i <= 4; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 2) {
                numero[1] = categoria[1];
                System.out.println("|                            " + numero[1] + "                          |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int j = 0; i >= 5 && i <= 19; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 3) {
                numero[2] = categoria[2];
                System.out.println("|                            " + numero[2] + "                           |");
                System.out
                        .println("-----------------------------------------------------------------------------------");
                ;

                for (int a = 0; i >= 20 && i <= 26; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "     |");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 4) {
                numero[3] = categoria[3];
                System.out.println("|                            " + numero[3] + "                     |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int b = 0; i >= 27 && i <= 38; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 5) {
                numero[4] = categoria[4];
                System.out.println("|                            " + numero[4] + "                  |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int c = 0; i >= 39 && i <= 43; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

        }
        System.out.println("-----------------------------------------------------------------------------------");
        ;

        RetornarCatalogo(2); // Pasivos
        RetornarCatalogo(0);// Cabecera de tabla

        for (int i = 0; i < cuentasSistema.length; i++) {

            if (cuentasSistema[i].getClasificacion() == 6) {

                numero[5] = categoria[5];
                System.out.println("|                            " + numero[5] + "                       |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int d = 0; i >= 44 && i <= 57; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 7) {
                numero[6] = categoria[6];
                System.out.println("|                            " + numero[6] + "                          |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int e = 0; i >= 58 && i <= 62; i++) {
                    System.out.println("|   " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 8) {
                numero[7] = categoria[7];
                System.out.println("|                            " + numero[7] + "                        |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int f = 0; i >= 63 && i <= 64; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

        }
        System.out.println("-----------------------------------------------------------------------------------");

        RetornarCatalogo(3);
        RetornarCatalogo(0);

        for (int i = 0; i < cuentasSistema.length; i++) {

            if (cuentasSistema[i].getClasificacion() == 9) {
                numero[8] = categoria[8];
                System.out
                        .println("|                            " + numero[8] + "                                   |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int g = 0; i >= 65 && i <= 69; i++) {
                    System.out.println("|    " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }

            if (cuentasSistema[i].getClasificacion() == 10) {
                numero[9] = categoria[9];
                System.out.println(
                        "|                            " + numero[9] + "                                        |");
                System.out
                        .println("-----------------------------------------------------------------------------------");

                for (int h = 0; i >= 70 && i <= 72; i++) {
                    System.out.println("|   " + cuentasSistema[i].getReferenciaCuenta() + "       |        "
                            + cuentasSistema[i].getNombreCuenta() + "|");
                    System.out.println(
                            "-----------------------------------------------------------------------------------");

                }
            }
        }
    }

    public void agregarCuentasUsuario() throws Exception {
        int opc = 1;

        // if(cuentasUsuarioTemp == null){
        // int nuevoLengt = cuentasUsuarioFinalLength;
        // cuentasUsuarioTemp = new Cuenta[nuevoLengt];
        // cuentasUsuarioTemp = cuentasUsuarioFinal;
        // }
        cuentasUsuarioTemp = new Cuenta[1];
        clasificacionesTemp = new boolean[10];
        clasificacionesFinal = new boolean[10];

        System.out.println(
                "Digite los códigos de cuenta que desea utilizar pulsando enter para agregar. Digite 0 para terminar");
        do {
            try {
                String cuenta;
                do {
                    System.out.print("Agregar cuenta: ");
                    sc.nextLine();
                    cuenta = sc.nextLine();
                    cuenta = cuenta.replaceAll(" ", "");

                } while (cuenta.equals(""));

                if (Integer.parseInt(cuenta) == 0) {
                    break;
                }

                boolean encontradoSistema = false;
                boolean encontradoUsuario = false;
                boolean repetida = false;

                Cuenta cuentaSistema = new Cuenta();

                if (cuentasUsuarioTemp.length > 1) {
                    if (cuentasUsuarioFinal != null) {
                        for (int i = 0; i < cuentasUsuarioFinal.length - 1; i++) {
                            if (cuentasUsuarioFinal[i].getReferenciaCuenta().equals(cuenta)) {
                                encontradoUsuario = true;
                                repetida = true;
                                encontradoSistema = false;
                                break;
                            }
                        }
                    }

                    for (int i = 0; i <= cuentasUsuarioTemp.length - 2; i++) {
                        if (cuentasUsuarioTemp[i].getReferenciaCuenta().equals(cuenta)) {
                            encontradoUsuario = true;
                            repetida = true;
                            encontradoSistema = false;
                            break;
                        }
                    }

                    if (encontradoUsuario == false) {
                        for (Cuenta cs : cuentasSistema) {
                            if (cuenta.equals(cs.getReferenciaCuenta())) {
                                encontradoSistema = true;
                                encontradoUsuario = false;
                                repetida = false;
                                cuentaSistema = cs;
                                break;
                            }
                        }
                    }

                    if (encontradoSistema == true && encontradoUsuario == false && repetida == false) {
                        cuentasUsuarioTemp[cuentasUsuarioTemp.length - 1] = cuentaSistema;
                        cuentasUsuarioTemp = Arrays.copyOf(cuentasUsuarioTemp, cuentasUsuarioTemp.length + 1);
                    }
                } else if (cuentasUsuarioTemp.length == 1) {
                    if (cuentasUsuarioFinal != null) {
                        for (int i = 0; i < cuentasUsuarioFinal.length - 1; i++) {
                            if (cuentasUsuarioFinal[i].getReferenciaCuenta().equals(cuenta)) {
                                encontradoUsuario = true;
                                repetida = true;
                                encontradoSistema = false;
                                break;
                            }
                        }
                    }
                    if (!encontradoUsuario) {
                        for (Cuenta cs : cuentasSistema) {
                            if (cuenta.equals(cs.getReferenciaCuenta())) {
                                cuentasUsuarioTemp[0] = cs;
                                cuentaSistema = cs;
                                cuentasUsuarioTemp = Arrays.copyOf(cuentasUsuarioTemp, cuentasUsuarioTemp.length + 1);
                                encontradoSistema = true;
                                encontradoUsuario = false;
                                repetida = false;
                                break;
                            }
                        }
                    }
                }
                if (encontradoSistema == true && encontradoUsuario == false && repetida == false) {
                    System.out.println("La cuenta " + cuentaSistema.getReferenciaCuenta() + ": "
                            + cuentaSistema.getNombreCuenta().replace("  ", "") + " ha sido registrada.");
                } else if (encontradoUsuario == true && repetida == true && encontradoSistema == false) {
                    JOptionPane.showMessageDialog(null, "La cuenta ya ha sido registrada.", "Error",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "La cuenta no ha sido encontrada. Por favor, digite nuevamente.", "Error",
                            JOptionPane.WARNING_MESSAGE);
                }

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "La cuenta no ha sido encontrada. Por favor, digite nuevamente.",
                        "Error", JOptionPane.WARNING_MESSAGE);
            }

        } while (opc == 1);

        cuentasUsuarioTemp = Arrays.copyOfRange(cuentasUsuarioTemp, 0, cuentasUsuarioTemp.length - 1);

        cuentasUsuarioTemp = ordenarCuentas(cuentasUsuarioTemp);

        Cargando(1);
        System.out.println("--------------------------------------------------------");
        System.out.println("\nEstas son las cuentas que se agregarán al manual contable según su clasificación.");
        VerificarClasificacionesExistentes(cuentasUsuarioTemp, clasificacionesTemp, cabecerasClasificacionTemp);
        ImprimirManualContable(cuentasUsuarioTemp, clasificacionesTemp, clasificacionesTemp);

        System.out
                .println("¿Desea guardar los cambios en las cuentas que han sido registradas en el manual contable?");
        System.out.println("1. SI (Guardar y Cerrar)  2. Modificar cuentas");

        // Cargando(1);
        int opcCatalogoCambios = sc.nextInt();

        switch (opcCatalogoCambios) {

        case 1: {
            int cuentasNuevas = cuentasUsuarioTemp.length;
            if (cuentasUsuarioFinalLength == 0) {
                cuentasUsuarioFinal = new Cuenta[cuentasUsuarioTemp.length];
                cuentasUsuarioFinal = cuentasUsuarioTemp;
                cuentasUsuarioFinalLength = cuentasUsuarioFinal.length;
                cuentasUsuarioTemp = null;
            } else {
                int cantidadCuentasViejas = cuentasUsuarioFinal.length;
                int cantidadCuentasNuevas = cuentasUsuarioTemp.length;
                cuentasUsuarioFinal = Arrays.copyOf(cuentasUsuarioFinal, cantidadCuentasViejas + cantidadCuentasNuevas);
                for (int i = 0; i < cantidadCuentasNuevas; i++) {
                    if (cuentasUsuarioTemp[i] != null) {
                        int posicionAInsertar = cuentasUsuarioFinal.length - (cantidadCuentasNuevas - i);
                        cuentasUsuarioFinal[posicionAInsertar] = cuentasUsuarioTemp[i];
                    }
                }
                cuentasUsuarioTemp = null;
            }
            for (int i = 0; i < cuentasNuevas; i++) {
                cuentasUsuarioFinal = ordenarCuentas(cuentasUsuarioFinal);
            }
            break;
        }

        }
        return;
    }

    public void RetornarCuentasElegidas(Cuenta c, int cat, int pantalla) {

        switch (pantalla) {
        case 1: {
            System.out.println("|                            " + categoria[cat] + "                          |");
            System.out.println("-----------------------------------------------------------------------------------");
            break;
        }
        case 2: {
            System.out.println("|    " + c.getReferenciaCuenta() + "       |        " + c.getNombreCuenta() + "|");
            System.out.println("-----------------------------------------------------------------------------------");
            break;
        }
        }
    }

    public void RetornarCatalogo(int opc) {
        switch (opc) {
        case 1: { // Caso activo
            System.out.println();
            System.out.println("                            *****ACTIVOS*****                         ");
            System.out.println();
            System.out.println();
            break;
        }

        case 2: {
            System.out.println();
            System.out.println("                            *****PASIVOS*****                          ");
            System.out.println();
            break;
        }

        case 3: {
            System.out.println();
            System.out.println("                            *****CAPITAL*****                          ");
            System.out.println();
            break;
        }

        default: {
            System.out.println("-----------------------------------------------------------------------------------");
            System.out.println("|No. de Cuenta  |---------------- Nombre de Cuenta --------------------------------|");
            System.out.println("-----------------------------------------------------------------------------------");
        }
        }
    }

    private Cuenta[] ordenarCuentas(Cuenta cuentas[]) {
        int size = cuentas.length - 1;
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < cuentas.length - 1; j++) {
                if (cuentas[i].getReferenciaCuenta().compareTo(cuentas[j].getReferenciaCuenta()) > 0) {
                    Cuenta temp = cuentas[i];
                    cuentas[i] = cuentas[j];
                    cuentas[j] = temp;
                }
            }
        }
        return cuentas;
    }

    public boolean CatalogoLleno() {

        ClasificacionesGenerales[0] = false;
        ClasificacionesGenerales[1] = false;
        ClasificacionesGenerales[2] = false;
        for (Cuenta cu : cuentasUsuarioFinal) {
            if (cu != null) {
                if (cu.getClasificacion() >= 1 && cu.getClasificacion() <= 5) {
                    ClasificacionesGenerales[0] = true;
                } else if (cu.getClasificacion() > 5 && cu.getClasificacion() <= 8) {
                    ClasificacionesGenerales[1] = true;
                } else if (cu.getClasificacion() > 8 && cu.getClasificacion() <= 10) {
                    ClasificacionesGenerales[2] = true;
                }
            }
        }
        if (ClasificacionesGenerales[0] == true && ClasificacionesGenerales[1] == true
                && ClasificacionesGenerales[2] == true) {
            return true;
        } else {
            return false;
        }
    }

    private void ImprimirManualContable(Cuenta[] cuentas, boolean[] clasificaciones, boolean[] cabecerasClasificacion) {
        for (int i = 0; i < 3; i++) {
            RetornarCatalogo(i + 1);
            RetornarCatalogo(0);
            switch (i) {
            case 0: {
                for (int j = 0; j < 5; j++) {
                    if (clasificaciones[j]) {
                        for (Cuenta c : cuentas) {
                            if (c != null && c.getClasificacion() == j + 1) {
                                if (cabecerasClasificacion[j]) {
                                    RetornarCuentasElegidas(c, j, 1);
                                    RetornarCuentasElegidas(c, j, 2);
                                    cabecerasClasificacion[j] = false;
                                } else {
                                    RetornarCuentasElegidas(c, j, 2);
                                }
                            }
                        }
                    }
                }
                break;
            }

            case 1: {
                for (int j = 5; j < 8; j++) {
                    if (clasificaciones[j]) {
                        for (Cuenta c : cuentas) {
                            if (c != null && c.getClasificacion() == j + 1) {
                                if (cabecerasClasificacion[j]) {
                                    RetornarCuentasElegidas(c, j, 1);
                                    RetornarCuentasElegidas(c, j, 2);
                                    cabecerasClasificacion[j] = false;
                                } else {
                                    RetornarCuentasElegidas(c, j, 2);
                                }
                            }
                        }
                    }
                }
                break;
            }

            case 2: {
                for (int j = 8; j < 10; j++) {
                    if (clasificaciones[j]) {
                        for (Cuenta c : cuentas) {
                            if (c != null && c.getClasificacion() == j + 1) {
                                if (cabecerasClasificacion[j]) {
                                    RetornarCuentasElegidas(c, j, 1);
                                    RetornarCuentasElegidas(c, j, 2);
                                    cabecerasClasificacion[j] = false;
                                } else {
                                    RetornarCuentasElegidas(c, j, 2);
                                }
                            }
                        }
                    }
                }
                break;
            }
            }
        }
    }

    public void MostrarManualContable(Cuenta[] cuentasUsuario, boolean[] clasificaciones, boolean[] cabeceras) {
        if (cuentasUsuario != null) {
            System.out.println("                Estas son las cuentas en su manual contable:");
            System.out.println("");
            System.out.println("                           *****CATALOGO DE CUENTA*****                    ");
            System.out.println("");
            VerificarClasificacionesExistentes(cuentasUsuario, clasificaciones, cabeceras);
            ImprimirManualContable(cuentasUsuario, clasificaciones, cabeceras);
        } else {
            JOptionPane.showMessageDialog(null, "El manual contable aún no ha sido especificado.", "Error",
                    JOptionPane.WARNING_MESSAGE);
        }

    }

    private void VerificarClasificacionesExistentes(Cuenta[] cuentas, boolean[] clasificaciones,
            boolean[] cabecereraClasificacion) {
        for (Cuenta cu : cuentas) {
            if (cu != null) {
                switch (cu.getClasificacion()) {
                case 1:
                    clasificaciones[0] = true;
                    break;
                case 2:
                    clasificaciones[1] = true;
                    break;
                case 3:
                    clasificaciones[2] = true;
                    break;
                case 4:
                    clasificaciones[3] = true;
                    break;
                case 5:
                    clasificaciones[4] = true;
                    break;
                case 6:
                    clasificaciones[5] = true;
                    break;
                case 7:
                    clasificaciones[6] = true;
                    break;
                case 8:
                    clasificaciones[7] = true;
                    break;
                case 9:
                    clasificaciones[8] = true;
                    break;
                case 10:
                    clasificaciones[9] = true;
                    break;
                default:
                    break;
                }
            }
        }

        cabecereraClasificacion = clasificaciones;
    }

    public void Cargando(int veces) throws Exception {
        Thread main = new Thread();
        for (int j = 0; j < veces; j++) {
            System.out.print("\033[H\033[2J");
            System.out.flush();

            System.out.print("                             ");
            for (int i = 0; i < 10; i++) {
                main.sleep(250);
                System.out.print(".");
            }
            main.sleep(500);
        }
    }

    public void eliminarCuentasUsuario() throws Exception {
        System.out.println("Digite el número de cuenta que desea eliminar");
        sc.nextLine();
        String cuenta = sc.nextLine();
        boolean encontrado = false;
        int j = 0;
        for (Cuenta c : cuentasUsuarioFinal) {
            if (c.getReferenciaCuenta().equals(cuenta)) {
                encontrado = true;
                System.out.println("¿Está seguro que desea eliminar la cuenta " + c.getReferenciaCuenta() + "   :  " +
                        c.getNombreCuenta() + "   ?");
                System.out.println("1. SI  2. NO");
                int opc = sc.nextInt();

                switch (opc) {
                case 1: {
                    Cuenta[] cuentasParte1 = Arrays.copyOfRange(cuentasUsuarioFinal, 0, j);
                    Cuenta[] cuentasParte2 = Arrays.copyOfRange(cuentasUsuarioFinal, j + 1, cuentasUsuarioFinal.length);
                    cuentasUsuarioFinal = cuentasParte1;

                    int cuentasNuevas = cuentasParte2.length;
                    if (cuentasUsuarioFinalLength == 0) {
                        cuentasUsuarioFinal = new Cuenta[cuentasParte2.length];
                        cuentasUsuarioFinal = cuentasParte2;
                        cuentasUsuarioFinalLength = cuentasUsuarioFinal.length;
                    } else {
                        int cantidadCuentasViejas = cuentasUsuarioFinal.length;
                        int cantidadCuentasNuevas = cuentasParte2.length;
                        cuentasUsuarioFinal = Arrays.copyOf(cuentasUsuarioFinal,
                                cantidadCuentasViejas + cantidadCuentasNuevas);
                        for (int i = 0; i < cantidadCuentasNuevas; i++) {
                            if (cuentasParte2[i] != null) {
                                int posicionAInsertar = cuentasUsuarioFinal.length - (cantidadCuentasNuevas - i);
                                cuentasUsuarioFinal[posicionAInsertar] = cuentasParte2[i];
                            }
                        }
                    }
                    for (int i = 0; i < cuentasNuevas; i++) {
                        cuentasUsuarioFinal = ordenarCuentas(cuentasUsuarioFinal);
                    }
                    break;
                }

                case 2: {
                    System.out.println("¿Desea eliminar otra cuenta?");
                    System.out.println("1. SI  2. NO");
                    int opc2 = sc.nextInt();

                    switch (opc2) {
                    case 1: {
                        eliminarCuentasUsuario();
                        break;
                    }

                    case 2: {
                        break;
                    }
                    }
                    break;
                }
                }
                break;
            }
            j++;
            if (encontrado == true) {
                break;
            }

        }

        if (!encontrado) {
            JOptionPane.showMessageDialog(null, "!La cuenta no ha sido encontrada en el manual contable!", "Error",
                    JOptionPane.WARNING_MESSAGE);
        }
    }
}
