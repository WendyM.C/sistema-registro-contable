/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author KAROL_PC
 */
public class Cuenta {
    String referenciaCuenta;
    String nombreCuenta;
    int clasificacion;

    public Cuenta() {
    }

    public Cuenta(String referenciaCuenta, String nombreCuenta, int clasificacion){
        this.referenciaCuenta = referenciaCuenta;
        this.nombreCuenta = nombreCuenta;
        this.clasificacion = clasificacion;
        
    } 
    public String getReferenciaCuenta() {
        return referenciaCuenta;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }
     
    public int getClasificacion() {
        return clasificacion;
    }
    
    
}
